<?php

/**
 * @file
 * Documenting Opening hours hooks.
 */

/**
 * Allows modules to alter Opening hours instances before saving them.
 *
 * @param $updated_instance
 *   The updated Opening hours instance as an object.
 *
 * @param $instance
 *   The old Opening hours instance as an object. If this object is NULL,
 *   then it is a new instance that is getting saved.
 */
function hook_opening_hours_presave($updated_instance, $instance) {
  // No example.
}

/**
 * Allows modules to act before an Opening hours instance is deleted.
 *
 * @param $instance
 *   The Opening hours instance as an object.
 */
function hook_opening_hours_delete($instance) {
  // No example.
}
