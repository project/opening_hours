/**
 * @file
 * Extensions for JavaScript core prototypes.
 *
 * Polyfills missing functionality.
 */

(function () {
  'use strict';

  /**
   * Pad a number with a zero if less than 10.
   *
   * @param {Number} n Number to pad.
   *
   * @return {String} Padded number.
   */
  function pad(n) {
    if (n < 1) {
      return n;
    }

    return n < 10 ? '0' + n : n;
  }

  if (typeof Date.prototype.getISODate !== 'function') {

    /**
     * Format date in ISO 8601-format.
     *
     * @return {string} Date in ISO 8601 format.
     */
    Date.prototype.getISODate = function () {
      return this.getFullYear() + '-' + pad(this.getMonth() + 1) + '-' + pad(this.getDate());
    };
  }

}());
